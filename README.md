# AWX operator setup-config playbook 

This is a simple playbook that follows the basic steps in this [guide]() to install, setup, and configur AWX operator. 

Used to mock a data migration of AWX (docker setup) to AWX (kubernetes)  


# Resources 
- https://minikube.sigs.k8s.io/docs/start/
- github.com/geerlingguy/ansible-role-docker
- https://opensource.com/article/20/9/ansible-modules-kubernetes
- https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/#install-using-native-package-management
